<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Faker\Factory as Faker;

/**
 * Class DatabaseSeeder
 */
class DatabaseSeeder extends Seeder
{
    protected $products = [''];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $faker = Faker::create();
        foreach (range(1, 10) as $index) {
            DB::table('products')->insert([
                'title' => $faker->randomAscii,
                'description' => $faker->paragraph,
                'price' => rand(1, 100000),
            ]);
        }
    }
}
