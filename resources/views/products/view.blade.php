@extends('layouts.admin')

@section('content')
    <div class="row well">

        @include('products.components.tools')
    </div>
    <div id="products" class="row list-group">
        <div class="item  col-lg-12 col-xs-12">
            <div class="thumbnail">
                <div class="caption">
                    <div class="row caption">
                        <h2>
                            <strong>{{$product->title}}</strong>
                        </h2>


                    </div>
                    <div class="row">
                        <img class="group list-group-image" src="{{$product->image_url}}" alt="{{$product->title}}"/>
                    </div>

                    <div class="row">
                        <p class="group inner list-group-item-text">
                            {{$product->description}}
                        </p>
                    </div>
                    <div class="row">
                        <input readonly hidden name="id" value="{{$product->id}}">
                        <div class="row text-right">
                            <div class="col-xs-12 col-md-6">
                                <p class="lead price">{{$product->price_label}}</p>
                            </div>
                        </div>
                        <div class="row text-center">
                            <div class="col-xs-12 col-md-6">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


@endsection
