<div class="col-xs-4 col-lg-4 text-left vcenter">
    <strong>demo shop</strong>
</div>
<div class="col-xs-4 col-lg-4 text-left  vcenter">
    <a href="{{ action('StoreController@create', ['key' => md5(uniqid(mt_rand(), true))]) }}"
       class="btn btn-primary" role="button">Add new Product</a>
</div>
<div class="col-xs-4 col-lg-4 text-right vcenter pull-right">
    <div class="btn-group">
        <a href="#" id="list" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list"></span> List</a>
        <a href="#" id="grid" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th"></span> Grid</a>
    </div>
</div>
