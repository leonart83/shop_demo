@extends('layouts.admin')

@section('content')
    <div class="row well">
        @include('products.components.tools')
    </div>

    {!! $products->render() !!}

    <div id="products" class="row list-group">
        @foreach($products as  $product)

            <div class="item  col-xs-4 col-lg-4">

                <div class="thumbnail">
                    {!! Form::open(['action' =>
                                 [
                                 'StoreController@destroy', $product->id],
                                 'method' => 'delete',
                                 'class'=>'text-right'
                                 ]) !!}

                    {!! Form::submit('x', ['class'=>'btn btn-danger btn-xs','role'=>'button']) !!}
                    {!! Form::close() !!}
                    <img class="group list-group-image" style="width: 400px; height: 250px;"
                         src="{{$product->image_url}}"
                         alt="{{$product->title}}"/>
                    <div class="caption">
                        <h4 class="group inner list-group-item-heading">
                            {{$product->title}}</h4>
                        <p class="group inner list-group-item-text">
                            {{$product->description_preview}}
                        </p>
                        <div class="row">

                            <div class="col-xs-12 col-md-6">
                                <div class="text-left">
                                    <a class="btn btn-primary btn-xs"
                                       href="{{ url('store', [$product->id] )}}">View</a>
                                    <a class="btn btn-primary btn-xs"
                                       href="{{ url('store/'.$product->id.'/edit')}}">Edit</a>

                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <p class="price lead">{{$product->price_label}}</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        @endforeach
    </div>
@endsection
