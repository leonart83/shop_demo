@extends('layouts.admin')

@section('content')
    <div class="row well">

        {!! Form::open(['action' => ['StoreController@destroy', $product->id], 'method' => 'delete']) !!}
        <a class="btn btn-primary btn-sm"
           href="{{ url('store/' )}}">Go Back</a>
        {!! Form::submit('Delete Product', ['class'=>'btn btn-danger btn-sm pull-right','role'=>'button']) !!}
        {!! Form::close() !!}
        <div id="products" class="row list-group">
            <div class="item  col-xs-12 col-lg-12">
                <div class="caption">
                    <div class="row caption">
                        <h2>
                            <strong>{{$product->title}}</strong>
                        </h2>
                        {!! Form::open( array('action'=> ['StoreController@update',$product->id] ,'method'=>'PATCH','files'=>true)) !!}
                        {!! Form::hidden('id', $product->id) !!}
                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            {{ Form::label('title', 'Title') }}
                            {{ Form::text('title', $product->title, ['class' => 'form-control' , 'placeholder' => 'product title', 'required']) }}
                            {{ $errors->first('title', '<span class="help-block">:messages</span>')}}
                        </div>

                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            {{ Form::label( 'description:' ) }}
                            {{ Form::text( 'description', $product->description , ['class' => 'form-control' , 'placeholder' => 'product description', 'required']) }}
                            {{ $errors->first('description', '<span class="help-block">:messages</span>')}}

                        </div>
                        <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                            {{ Form::label( 'price:' ) }}
                            {{ Form::number( 'price', $product->price  , ['class' => 'form-control', 'step'=>"any"  , 'placeholder' => 'product price']) }}
                            {{ $errors->first('price', '<span class="help-block">:messages</span>')}}
                        </div>
                        <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                            {!! Form::file('image',['placeholder'=>'image','class'=>'form-control','id'=>'image']) !!}

                            <div class="thumbnail">
                                <img class="group list-group-image" src="{{$product->image_url}}"
                                     alt="{{$product->title}}"/>
                                {{ $errors->first('image', '<span class="help-block">:messages</span>')}}
                            </div>
                        </div>

                        {!! Form::hidden( 'created_at', $product->created_at) !!}
                        {!! Form::hidden( 'updated_at', $product->updated_at) !!}
                        {!! Form::submit('Save',['class'=>'btn btn-primary']) !!}
                        {!! Form::close() !!}

                    </div>

                    <div class="row">
                        <p class="group inner list-group-item-text">
                            {{$product->description}}
                        </p>
                    </div>
                    <div class="row">
                        <div class="row text-right">
                            <div class="col-xs-12 col-md-6">
                                <p class="lead price">{{$product->price_label}}</p>
                            </div>
                        </div>
                        <div class="row text-center">
                            <div class="col-xs-12 col-md-6">

                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

@endsection
