<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 *
 * @package App
 */
class Product extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['title', 'description', 'price'];

    /**
     * @var array
     */
    protected $appends = ['price_label', 'description_preview', 'image_url'];
    /**
     * @var array
     */
    protected $casts = ['price' => 'float'];

    protected $rules = [
        'id' => 'required|int',
        'title' => 'required',
        'description' => 'required',
        'price' => 'required|numeric|min:0',
        'image' => 'sometimes',
    ];

    /**
     * @var array
     */
    protected $rulesMessages = [
        'id.required' => 'Id is required',
        'title.required' => 'Title is required',
        'description.required' => 'Description is required',
        'price.required' => 'Price is required',
        'price.numeric' => 'Price must be a number',
        'price.min' => 'Price cannot be lower or equal zero',
        'image.required' => 'Please upload an image',
    ];

    public function getRules()
    {
        return $this->rules;
    }

    public function getRulesMessages()
    {
        return $this->rulesMessages;
    }

    protected function getPriceLabelAttribute()
    {
        return number_format($this->price, 2) . ' лв';
    }

    /**
     * @return string
     */
    public function getDescriptionPreviewAttribute()
    {
        return (strlen($this->description) > 127) ? substr($this->description, 0, 127) . '...' : $this->description;
    }

    /**Get Image Url for the product or a placeholder
     *
     * @return string
     */
    public function getImageUrlAttribute()
    {
        $image = isset($this->image) ? $this->image : 'placeholder.png';
        return asset('images/' . $image);
    }


}
