<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\File;

use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Class StoreController
 *
 * @package App\Http\Controllers
 */
class StoreController extends Controller
{
    /**
     * @return string
     *
     * method GET|HEAD
     */
    public function index()
    {
        $products = Product::paginate(6);
        return view('products.index', ['products' => $products]);

    }

    /**
     * @param  int
     * @return array
     *
     * method POST
     */
    public function store()
    {
        return Product::all()->toJson();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     *
     *  method GET|HEAD
     */
    public function create(Request $request)
    {
        $validator = Validator::make(
            $request->all(), [
            'key' => 'required|size:32'
        ]);

        if ($validator->fails()) {
            return redirect()->action('StoreController@index')->with('message', 'Invalid Key');
        }

        $product = (object)Product::create();
        return redirect()->action('StoreController@edit', [$product->id]);
    }

    /**
     * @param $id integer
     * @param $request Request
     * @return string
     * @throws ModelNotFoundException
     *
     * method GET
     */
    public function show($id, Request $request)
    {
        $product = null;
        try {
            $product = (object)Product::findOrFail($id)->toArray();
        } catch (ModelNotFoundException $e) {
            return response('Product not found', 404)
                ->header('Content-Type', 'application/json');
        }

        return view('products.view', ['product' => $product]);


    }


    /**
     * @param int $id
     * @param Request $request
     * @return string
     * @throws ModelNotFoundException
     *
     * method PUT|PATCH
     */
    public function update($id, Request $request)
    {
        $product = null;
        try {
            $product = Product::findOrFail($id);

            $validator = Validator::make($request->all(), $product->getRules(), $product->getRulesMessages());

            if ($validator->fails()) {
                return back()
                    ->withErrors($validator)
                    ->withInput();
            }
            $image = $request->file('image');
            if ($image) {
                $filename = time() . '.' . $image->getClientOriginalExtension();
                $path = public_path('images/' . $filename);
                Image::make($image->getRealPath())->resize(400, 250)->save($path);
                if (isset($product->image)) {
                    if (File::exists(public_path('images/' . $product->image))) {
                        File::delete(public_path('images/' . $product->image));
                    }
                }

                $product->image = $filename;
            }


            $product->fill($request->all());

            $result = $product->save();

        } catch (ModelNotFoundException $e) {
            return response('Product not found', 404)
                ->header('Content-Type', 'application/json');
        }
        return redirect('store')->with('result', $result);
    }

    /**
     *
     * @param $id
     * @param Request $request
     * @return string
     *
     * method GET|HEAD
     */

    public function edit($id)
    {
        $product = null;
        try {
            $product = Product::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response('Product not found', 404)
                ->header('Content-Type', 'application/json');
        }
        return view('products.edit', ['product' => $product]);
    }

    /**
     *
     * @param $id
     * @param Request $request
     * @return string
     *
     * method DELETE
     */
    public function destroy($id, Request $request)
    {

        $result = Product::destroy($id);
        return redirect('store')->with('result', $result);

    }
}
